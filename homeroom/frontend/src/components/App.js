import React from "react";
import ReactDOM from "react-dom";

import Header from "./Layout/Header";
import Dashboard from "./Courses/Dashboard";

import { Provider } from "react-redux";
import store from "../store";

function App() {
  return (
    <Provider store={store}>
      <Header />
      <Dashboard />
    </Provider>
  );
}

ReactDOM.render(<App />, document.getElementById("app"));
