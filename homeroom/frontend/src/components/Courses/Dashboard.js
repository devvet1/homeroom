import React from "react";

import Courses from "./Courses";
import Form from "./Form";

export default function Dashboard() {
  return (
    <>
      <Form />
      <Courses />
    </>
  );
}
