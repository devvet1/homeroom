import React, { useEffect } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { getCourses } from "../../actions/courses";

const monthNames = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export function Courses(props) {
  useEffect(() => props.getCourses(), []);

  return (
    <div className="container">
      <h1>Course List</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Subject</th>
            <th>Title</th>
            <th>Start</th>
          </tr>
        </thead>
        <tbody>
          {props.courses.map((course) => {
            let courseDate = new Date(course.start_date);
            return (
              <tr key={course.created_at}>
                <td>{course.subject}</td>
                <td>{course.title}</td>
                <td>
                  {`${
                    monthNames[courseDate.getUTCMonth()]
                  } ${courseDate.getUTCDate()}, ${courseDate.getFullYear()}`}
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
}

Courses.propTypes = {
  courses: PropTypes.array.isRequired,
};

const mapStateToProps = (state) => ({
  courses: state.courses.courses,
});

export default connect(mapStateToProps, { getCourses })(Courses);
