import axios from "axios";

import { GET_COURSES } from "./types";

// GET COURSES
export const getCourses = () => (dispatch) => {
  axios
    .get("http://localhost:8000/api/course")
    .then((res) => {
      dispatch({
        type: GET_COURSES,
        payload: res.data,
      });
    })
    .catch((err) => console.error(err));
};
