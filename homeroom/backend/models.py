from django.db import models
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def is_student(user):
    if user.user_role.lower() != "student":
        raise ValidationError(
            _('%(user) is not a student'),
            params={'user': user.username}
        )


def is_instructor(user):
    if user.user_role.lower() != "instructor":
        raise ValidationError(
            _('%(user) is not an instructor'),
            params={'user': user.username}
        )


class User(models.Model):

    class UserRoles(models.TextChoices):
        ADMIN = "admin", _("admin")
        INSTRUCTOR = "instructor", _("instructor")
        STUDENT = "student", _("student")

    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    email = models.EmailField(max_length=100, unique=True)
    username = models.CharField(max_length=20, unique=True)
    role = models.CharField(max_length=20, choices=UserRoles.choices)
    created_at = models.DateTimeField(auto_now_add=True)


class Course(models.Model):
    instructors = models.ManyToManyField(User, related_name="instructors")
    students = models.ManyToManyField(User, related_name="students")
    title = models.CharField(max_length=100)
    start_date = models.DateTimeField()
    end_date = models.DateTimeField()
    subject = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.CharField(max_length=20)


class Lesson(models.Model):
    title = models.CharField(max_length=100)
    courses = models.ManyToManyField(Course)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.CharField(max_length=20)


class LessonContent(models.Model):
    title = models.CharField(max_length=100)
    lessons = models.ManyToManyField(Lesson)
    content_type = models.CharField(max_length=100)
    location = models.CharField(max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.CharField(max_length=20)


class LessonActivity(models.Model):

    class ActivityType(models.TextChoices):
        TEST = 'Test', _('Test')
        QUIZ = 'Quiz', _('Quiz')
        ASSIGNMENT = 'Assignment', _('Assignment')

    title = models.CharField(max_length=100)
    activity_type = models.CharField(max_length=50, choices=ActivityType.choices)
    lessons = models.ManyToManyField(Lesson)
    is_graded = models.BooleanField()
    location = models.CharField(max_length=500, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.CharField(max_length=20)


class Grade(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)
    lesson_id = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    lesson_activity_id = models.ForeignKey(LessonActivity, on_delete=models.CASCADE)
    grade = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.CharField(max_length=20)


class ItemCompleted(models.Model):

    class ItemType(models.TextChoices):
        CONTENT = 'Content', _('Content')
        ACTIVITY = 'Activity', _('Activity')

    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    course_id = models.ForeignKey(Course, on_delete=models.CASCADE)
    lesson_id = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    item_type = models.CharField(max_length=50, choices=ItemType.choices)


class LessonPlan(models.Model):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)


class Attendance(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE ,validators=[is_student])
    date = models.DateTimeField()
    attended = models.BooleanField()
    note = models.CharField(max_length=2000, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    modified_by = models.CharField(max_length=20)


class Password(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    pass_hash = models.CharField(max_length=500)

