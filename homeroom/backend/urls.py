from django.conf.urls import url, include
from rest_framework import routers
from .api import *

router = routers.DefaultRouter(trailing_slash=False)
router.register('api/user', UserViewSet, 'user')
router.register('api/course', CourseViewSet, 'course')
router.register('api/lesson', LessonViewSet, 'lesson')
router.register('api/lessoncontent', LessonContentViewSet, 'lessoncontent')
router.register('api/lessonactivity', LessonActivityViewSet, 'lessonactivity')
router.register('api/lessonplan', LessonPlanViewSet, 'lessonplan')
router.register('api/grade', GradeViewSet, 'grade')
router.register('api/itemcompleted', ItemCompletedViewSet, 'itemcompleted')
router.register('api/attendance', AttendanceViewSet, 'attendance')
router.register('api/password', PasswordViewSet, 'password')

urlpatterns = router.urls
