from .models import *
from rest_framework import viewsets, permissions
from .serializers import *


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = UserSerializer


class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = CourseSerializer


class LessonViewSet(viewsets.ModelViewSet):
    queryset = Lesson.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = LessonSerializer


class LessonContentViewSet(viewsets.ModelViewSet):
    queryset = LessonContent.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = LessonContentSerializer


class LessonActivityViewSet(viewsets.ModelViewSet):
    queryset = LessonActivity.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = LessonActivitySerializer


class LessonPlanViewSet(viewsets.ModelViewSet):
    queryset = LessonPlan.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = LessonPlanSerializer


class GradeViewSet(viewsets.ModelViewSet):
    queryset = Grade.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = GradeSerializer


class ItemCompletedViewSet(viewsets.ModelViewSet):
    queryset = ItemCompleted.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = ItemCompletedSerializer

class AttendanceViewSet(viewsets.ModelViewSet):
    queryset = Attendance.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = AttendanceSerializer


class PasswordViewSet(viewsets.ModelViewSet):
    queryset = Password.objects.all()
    permission_classes = [
        permissions.AllowAny
    ]
    serializer_class = PasswordSerializer