# HomeRoom

## Installation
#### Clone Repository
```
git clone https://gitlab.com/devvet1/homeroom.git
cd ./homeroom
```
---
> This section can be skipped, however, using virtual environments is highly 
> recommended. If you do, then be sure to activate the environment each time you
> work on the project
#### Create a Virtual Environment
###### Install `virtualenv` globally

`pip install virtualenv`

###### Create virtual environment for the project

`virtualenv venv`

###### Activate project's virtual environment (from within project directory)

`source ./venv/bin/activate`

---
#### Install python dependencies
`pip3 install -r requirements.txt`

#### Instantiate local SQLite database
```
cd ./homeroom
python manage.py migrate
```

Current Database Model
![DataModel](./supportImages/models.png)

## Todo
- [ ] Post Endpoint Requirements
- [ ] Create REST endpoints
- [ ] Initialize Frontend Application
- [ ] A lot more